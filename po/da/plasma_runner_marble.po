# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-28 00:36+0000\n"
"PO-Revision-Date: 2011-12-22 21:52+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: plasmarunner.cpp:33
#, kde-format
msgid "Shows the coordinates :q: in OpenStreetMap with Marble."
msgstr "Viser koordinaterne :q: i OpenStreetMap med Marble."

#: plasmarunner.cpp:34
#, kde-format
msgid "Shows the geo bookmark containing :q: in OpenStreetMap with Marble."
msgstr "Viser geo-bogmærket som indeholder :q: i OpenStreetMap med Marble."

#: plasmarunner.cpp:54
#, kde-format
msgid "Show the coordinates %1 in OpenStreetMap with Marble"
msgstr "Vis koordinaterne %1 i OpenStreetMap med Marble"

#: plasmarunner.cpp:115
#, kde-format
msgid "Show in OpenStreetMap with Marble"
msgstr "Vis i OpenStreetMap med Marble"
